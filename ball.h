#pragma once

#include "screen.h"

enum Arguments {RADIUS,XSPEED,YSPEED,XPOSITION,YPOSITION,HELP,GRAVITY,FPS,FORM};

struct speed {
    double vx;
    double vy;
};
typedef struct speed speed;

struct position {
    double x;
    double y;
};

typedef struct position position;

struct Ball {
    position init_p;
    speed init_s;
    position p;
    speed s;
};
typedef struct Ball Ball;

void ball_move(struct Ball* ball,double* dt,const Screen *screen,
               int* arguments);

void ball_init(Ball*,const Screen*,
               int* arguments);

void ball_to_screen(const Screen*,const Ball*, int*);

