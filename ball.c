#include "ball.h"

#define _RADIUS 10
double squared(double x)
{
    return x*x;
}

//TODO replace
void arg_init(int arg, int*val,int* arguments)
{
    if(!val || !arguments) {
        fprintf(stderr,"Null parameters\n");
        return;
    }

    if(arguments[arg] != 1) {
        *val = arguments[arg];
    }
}

double fabs(double x)
{
    if(x > 0) return x; else return -x;
}

void ball_move(struct Ball* ball,double* dt,const Screen *screen,
        int* arguments)
{
    if(!ball || !dt || !screen) {
        fprintf(stderr,"Null parameters\n");
        return;
    }
    double g = -50;
    if(arguments[GRAVITY] != -1) {
        g = arguments[GRAVITY];
    }

    int radius = _RADIUS;
    if(arguments[RADIUS] != -1) {
        radius = arguments[RADIUS];
    }

    ball->s.vx = ball->init_s.vx + g* *dt;
    ball->s.vy = ball->init_s.vy;

    ball->p.x =  (ball->init_p.x) +  (ball->init_s.vx * *dt + g* (*dt) * (*dt)*0.5 );
    ball->p.y =  ball->init_p.y + (ball->init_s.vy *  *dt);

    int limit = radius+1;

    //low
    if(ball->p.x < limit) {
        ball->init_p.x  = limit;
        ball->init_p.y  = ball->p.y;
        ball->init_s.vx = ball->s.vx*(-0.8);
        ball->init_s.vy = ball->init_s.vy*(0.9);
        *dt = 0;
    }

    //right or left
    int temp;
    if((temp = (ball->p.y > screen->WIDTH-radius)) || ball->p.y < radius) {
        ball->init_p.x = ball->p.x;
        if(temp) ball->init_p.y = screen->WIDTH-radius; else ball->init_p.y = radius;
        ball->init_s.vx=ball->s.vx;
        ball->init_s.vy= ball->init_s.vy*(-0.5);
        *dt = 0;
    }


    //end
    if(fabs(ball->s.vx) < 1.0 && fabs(ball ->s.vy) < 0.2 && fabs(ball->p.x - limit)< 0.1) {
        ball_init(ball,screen,arguments);
    }
}


void ball_init(Ball* ball,const Screen* screen,
        int* arguments)
{
    if(!ball || !screen || !arguments) {
        fprintf(stderr,"Null parameters\n");
        return;
    }

    ball->init_s.vx = -30;
    ball->init_s.vy = 80;

    if(arguments[XSPEED]!= -1 ) {
        ball->init_s.vx = arguments[XSPEED];
    }

    if(arguments[YSPEED]!= -1 ) {
        ball->init_s.vy = arguments[YSPEED];
    }

    ball->init_p.x =  screen->HEIGHT;
    ball->init_p.y =  (double) screen->WIDTH/4;

    if( arguments[XPOSITION] != -1 ) {
        ball->init_p.x = arguments[XPOSITION];
    }
    if( arguments[YPOSITION] != -1 ) {
        ball->init_p.y = arguments[YPOSITION];
    }

    ball->p.x = ball->init_p.x;
    ball->p.y = ball->init_p.y;

    ball->s.vx = ball->init_s.vx;
    ball->s.vy = ball->init_s.vy;
}

void ball_to_screen(const Screen* screen, const Ball* ball,int* arguments)
{
    if(!screen || !ball || !arguments) {
        fprintf(stderr,"Null parameters\n");
        return;
    }

    int radius = _RADIUS;
    if(arguments[RADIUS] != -1) {
        radius = arguments[RADIUS];
    }

    int form = 'O';
    if(arguments[FORM] != -1) {
        form = arguments[FORM];
    }
    for (int i = 0; i < screen->HEIGHT; ++i) {
        for (int j = 0; j < screen->WIDTH; ++j) {
            double a = squared(screen->HEIGHT-i-ball->p.x);
            double b = squared(j-ball->p.y);
            if(a+b<squared(radius)) putchar(form);
            else if(i == screen->HEIGHT-1) putchar('_');
            else putchar(' ');
        }
        putchar('\n');
    }
}
