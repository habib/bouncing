    bouncing [arguments value]
    	--radius: radius of the ball
    	--xspeed: initial honrizontal speed
    	--yspeed: initial vertical speed
    	--xposition: initial position of the ball in the x axis
    	--yposition: initial position of the ball in the y axis
    	--help: print help
    	--gravity: the gravity of the environement
    	--fps: the number of image per second (doesn't work well)
    	--form: ASCII character for the ball
    example: bouncing --xspeed -10 --radius 5


![demo](./demo.gif)
