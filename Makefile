CC = gcc
CFLAGS += -std=c11  -Ofast

debug: CFLAGS += -Wextra -Wfloat-equal -Wshadow                         \
 -Wpointer-arith -Wbad-function-cast -Wcast-align -Wwrite-strings \
 -Wconversion -Wunreachable-code -Wall -pedantic -g

all: bouncing

debug: bouncing

bouncing: ball.o bouncing.o screen.o

ball.o: ball.c
bouncing.o: bouncing.c screen.h
screen.o: screen.c screen.h

clean::
	-@/bin/rm -f *.o *~ $(CHECK_TARGETS) bouncing
clear::
	-@/bin/rm -f *.o *~ $(CHECK_TARGETS) 



