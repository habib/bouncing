#include "screen.h"
#include "ball.h"
#include <string.h>
#include <time.h>

#define _FPS (1000.0)
#define nb_arg 9
//TODO
//controller la position pour recommencer le jeu
//rajouter argument pour le frottement
//dt == fps in ball.c


//--radius
//--xspeed
//--yspeed
//--xposition
//--yposition
//--help
//--fps
//--gravity
//
int main(int argc, char *argv[])
{
    argc--;
    argv++;
    const char* arguements[] =  {"--radius", "--xspeed","--yspeed",
        "--xposition", "--yposition","--help","--gravity","--fps","--form"
    };

    int arg_value[nb_arg];
    memset(arg_value,-1,nb_arg*sizeof(int));

    for (int i = 0; i < argc; ++i) {
        for(int j = 0; j<nb_arg; ++j) {
            if( !strcmp(argv[i],arguements[j])) {
                if(!strcmp("--help",argv[i]) || !strcmp("-h",argv[i])) {
                    arg_value[j] = 1;
                }else if (!strcmp("--form",argv[i])) {
                    arg_value[j] = *argv[++i];
                } else {
                    arg_value[j] = atoi(argv[++i]);
                    j = -1;
                }

            }
        }
    }

    if(arg_value[HELP] != -1) {
        printf(
                "bouncing [arguments value]\n"
                "\t--radius: radius of the ball\n"
                "\t--xspeed: initial honrizontal speed\n"
                "\t--yspeed: initial vertical speed\n"
                "\t--xposition: initial position of the ball in the x axis\n"
                "\t--yposition: initial position of the ball in the y axis\n"
                "\t--help: print help\n"
                "\t--gravity: the gravity of the environement\n"
                "\t--fps: the number of image per second (doesn't work well)\n"
                "\t--form: ASCII character for the ball\n"
                "example: bouncing --xspeed -10 --radius 5\n"
              );
        return 0;
    }

    Screen screen;
    screen_init(&screen);

    Ball ball;
    ball_init(&ball,&screen,arg_value);

    double fps = _FPS;

    if(arg_value[FPS] != -1 ) {
        fps = arg_value[FPS];
    }


    double __seconds = 1.0/fps;

    double iterations = 0;

    while(1) {
        //ball_move(&ball,(__seconds),&screen,arg_value);
        ball_move(&ball,&iterations,&screen,arg_value);
        iterations+=__seconds;
        ball_to_screen(&screen,&ball,arg_value);
        screen_clear(&screen);
        usleep((__useconds_t) ((__seconds)*1000000.0));
    }

    return 0;
}
