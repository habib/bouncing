#pragma once
#define _XOPEN_SOURCE 500

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/ioctl.h>

struct Screen {
    int HEIGHT;
    int WIDTH;
};

typedef struct Screen Screen;

void screen_init(Screen*);

void screen_clear(Screen*);

