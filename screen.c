#include "screen.h"

static void back(int* WIDTH,int*HEIGHT)
{
    printf("\x1b[%dD", *WIDTH);
    printf("\x1b[%dA", *HEIGHT);
}

void screen_init(Screen* screen)
{
    if(!screen) {
        fprintf(stderr,"Null parameters\n");
        return;
    }

    struct winsize w;
    ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);

    screen->HEIGHT = w.ws_row-2;
    screen->WIDTH = w.ws_col;
}

void screen_clear(Screen* screen)
{
    if(!screen) {
        fprintf(stderr,"Null parameters\n");
        return;
    }

    int WIDTH =(int) screen->WIDTH;
    int HEIGHT =(int) screen->HEIGHT;
    back(&WIDTH,&HEIGHT);
}
